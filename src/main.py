
import os
import json
import logging
import requests
from datetime import datetime
from googleapiclient import discovery
from google.cloud import storage 


def trigger_analysis(data, context):
    """
    :param data:
    :param context:
    :return:
    """

    print('Event ID: {}'.format(context.event_id))
    print('Event type: {}'.format(context.event_type))
    print('Bucket: {}'.format(data['bucket']))
    print('File: {}'.format(data['name']))
    print('Metageneration: {}'.format(data['metageneration']))
    print('Created: {}'.format(data['timeCreated']))
    print('Updated: {}'.format(data['updated']))

    file_name = data['name']
    bucket_name = data['bucket']
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.get_blob(file_name)
    file_size_gb = blob.size/1e+09
    machine_size_gb = file_size_gb * 3

    print("Create machine for file: %s; bucket_name: %s; filesize: %s; machine size: %s" % 
            (file_name, bucket_name, file_size_gb, machine_size_gb))

    # create vm
    operation = create_instance(file_name, bucket_name, file_size_gb, machine_size_gb)
    print(operation)


def create_instance(file_name, bucket_name, file_size_gb, machine_size_gb=None):

    # get variables from env
    project_id = os.environ.get('GCLOUD_PROJECT')
    zone = os.environ.get('INSTANCE_ZONE')
    docker_image = os.environ.get('DOCKER_IMAGE')
    output_bucket_name = os.environ.get('OUTPUT_BUCKET')

    # create timestamp and instance
    compute = discovery.build('compute', 'v1')
    now = datetime.now()
    stamp = now.strftime("%d%m%Yt%H%M")
    instance_name = "demo-instance-%s" % stamp
    print("Creating instance with name %s" % instance_name)

    # Get the latest Ubuntu image
    image_response = compute.images().getFromFamily(project='cos-cloud', family="cos-stable").execute()
    source_disk_image = image_response['selfLink']
    print("Found container image for creating the instance: %s" % source_disk_image)

    # Configure the machine
    machine_type = "zones/%s/machineTypes/n1-standard-1" % zone

    # prepare the container command
    args = ["/tools/run_analysis.py", "--file_name", "'%s'" % file_name, 
                                 "--bucket_name", "'%s'" % bucket_name, 
                                 "--output_bucket", "'%s'" % output_bucket_name,
                                 "--instance_name", "'%s'" % instance_name, 
                                 "--zone", "'%s'" % zone,
                                 "--project_id", "'%s'" % project_id]
    delim = '\n        - '
    command = "%s%s" % (delim, delim.join(args))

    if machine_size_gb is None:
        machine_size_gb = (file_size_gb * 4) + 10

    # prepare metadata for creating the VM
    metadata_value = "spec:\n  containers:\n    " \
                     "- name: %s\n      " \
                     "image: '%s'\n      " \
                     "command:\n        - python3\n      " \
                     "args:%s\n      " \
                     "stdin: false\n      " \
                     "tty: false\n  " \
                     "restartPolicy: Never\n\n" % (instance_name, docker_image, command)

    config = {
        'name': instance_name,
        'machineType': machine_type,

        # Specify the boot disk and the image to use as a source.
        'disks': [
            {
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': source_disk_image,
                },
                'deviceName': instance_name,
                'diskSizeGb': machine_size_gb
            }
        ],

        # Specify a network interface with NAT to access the public internet.
        'networkInterfaces': [{
            'network': 'global/networks/default',
            'accessConfigs': [
                {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT', "networkTier": "STANDARD"}
            ]
        }],

        # Allow the instance to access cloud storage and logging.
        'serviceAccounts': [{
            'email': 'default',
            'scopes': [
                "https://www.googleapis.com/auth/datastore",
                "https://www.googleapis.com/auth/compute",
                "https://www.googleapis.com/auth/servicecontrol",
                "https://www.googleapis.com/auth/service.management.readonly",
                "https://www.googleapis.com/auth/logging.write",
                "https://www.googleapis.com/auth/monitoring.write",
                "https://www.googleapis.com/auth/trace.append",
                "https://www.googleapis.com/auth/devstorage.read_write"
            ]
        }],

        # Metadata is readable from the instance and allows you to
        # pass configuration from deployment scripts to instances.
        'metadata': {
            'items': [
                {
                    "key": "gce-container-declaration",
                    "value": metadata_value
                },
                {
                    "key": "google-logging-enabled",
                    "value": "true"
                }
            ]
        },

        'labels': {
            "container-vm": image_response['name']
        },

        # Scheduling.preemptible deifines whether the VM is preemptible or not
        'scheduling': {
            "onHostMaintenance": "MIGRATE",
            "automaticRestart": True,
            "preemptible": False
        }
    }

    operation = compute.instances().insert(project=project_id, zone=zone, body=config).execute()

    return operation

