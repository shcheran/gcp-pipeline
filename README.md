
## GCP Pipeline for processing large files

The pipeline is aimed on processing of large files. Typically, the file can be processed inside the serverless Cloud Function triggered automatically once the file is added to the Cloud Storage. However, if the file needs localization in order to be analysed, Cloud Functions might not be the best choise if the size of the file is larger than 2GB which is the max limit of space that can be allocated for the Cloud Funtion. In this implementation, Cloud Function is triggered to create a Compute Engine virtual machine running the user-created docker in order to run the analysis for the file inside this virtual machine. The diagram of the pipeline is given below.


![alt text](diagram.png "Workflow Diagram")


### Creating Google Cloud infrastructure

I will be using [Google Cloud SDK](https://cloud.google.com/sdk/docs/quickstart) for the deployment of all Google services required for the pipeline.


**1. Create Cloud Storage buckets.**
We will create two buckets, one for keeping the inputs and one for keeping the outputs of the automatic analysis, using use gcloud SDK command:
```
gsutil mb -p PROJECT_ID -c STORAGE_CLASS -l BUCKET_LOCATION -b on gs://BUCKET_NAME
```
Here:

```
-p: Specifies the project in which bucket will be created. For example, PROJECT_ID.
-c: Specifies the storage class of the created bucket. For example, STANDARD.
-l: Specifies the location of the created bucket. For example, EUROPE-WEST1 region.
-b: Specifies whether uniform bucket-level access for the bucket should be enabled.
```

**2. Create Service Account for running the VM or use default compute engine account.** We will use default compute engine service account being in the format `<PROJECT_NUMBER>-compute@developer.gserviceaccount.com` (https://cloud.google.com/iam/docs/service-accounts#default). You can read more on how to create service accounts at https://developers.google.com/android/management/service-account.


**3. Create a docker image for analysing the data.**
Download docker tool if you don't have one. Then, authenticate to the docker using google docker helper tool: 
```
$ gcloud auth configure-docker --project=<PROJECT_ID>
```
Enable Google Container Registry API:
```
$ gcloud services enable containerregistry.googleapis.com
```
Write a Dockerfile. The Dockerfile should contain the script that will be running on Compute Engine virtual machine with the data added to the bucket. In this example the sequencing data is used with the subsequent execution of sequencing reads quality analysis. The Dockerfile specified in `docker/Dockerfile` is built based on Ubuntu 18.04 image and includes installation of tools required for running read quality analysis: fastqc and multiqc. 

The Dockerfile `docker/Dockerfile` is as follows:

```
# Set the base image to Ubuntu
FROM ubuntu:18.04

# Install dependencies and wget
RUN apt-get update \
	&& apt-get install -y build-essential \
	&& apt-get install -y python3-pip \
    && apt-get install -y fastqc

RUN mkdir /tools

COPY ./requirements.txt /tools/
RUN pip3 install -r /tools/requirements.txt

COPY ./run_analysis.py /tools/
```
In addition, a python script `docker/run_analysis.py` performs the following:

    - Localizes data to the VM,
    - Runs analysis tools,
    - Copies results to the output bucket,
    - Deletes the VM once all previous steps are finished.

The script takes the following parameters as inputs:

```
--file_name: Name of the file for which the analysis is performed;
--bucket_name: Name of the Cloud Storage bucket in which the file is located;
--instance_name: Name of the  Compute Engine instance that will be created in the project for running analysis;
--output_bucket: Name of the Cloud Storage bucket in which outputs will be saved;
--zone: Zone in which Compute Engine instance will be created;
--project_id: Project ID.
```

Variables `--file_name` and `--bucket_name` are passed to the cloud function automatically by the trigger, and variables `--output_bucket` and `--zone` are stored as environment varibales to the function. Variable `project_id` is obtained from the cloud function native environment variable GCLOUD_PROJECT.

Build a docker image:
```
$ docker build --tag=<YOUR_DOCKER_IMAGE_NAME>:<TAG> .
```
Once the docker image is built, re-name it to Google Container Registry format:
```
$ docker tag <YOUR_DOCKER_IMAGE_NAME>:<TAG> <HOSTNAME>/<PROJECT-ID>/<YOUR_DOCKER_IMAGE_NAME>:<TAG>
```
  where 
  	- `<HOSTNAME>` is name of host in Google Cloud Container registry, e.g. eu.gcr.io;
	- `<PROJECT-ID>` is Google Cloud project id;
	- `<YOUR_DOCKER_IMAGE_NAME>:<TAG>` is name you would like to give to your image in Container Registry.

Push image to Container Registry:
```
$ docker push <HOSTNAME>/<PROJECT-ID>/<YOUR_DOCKER_IMAGE_NAME>:<TAG>
```

**4. Create function with trigger** (https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/compute/api/create_instance.py). First, enable Cloud Functions API:
```
$ gcloud services enable cloudfunctions.googleapis.com
```
    
Next, enable Cloud Build API or visit using PROJECT_NUMBER (for more details visit https://console.developers.google.com/apis/api/cloudbuild.googleapis.com/overview?project=<PROJECT_NUMBER>)  
```
$ gcloud services enable cloudbuild.googleapis.com
```
    
Once APIs are activated, save cloud function code to e.g. `./src/main.py`. Script is creating a `n1-standard-1` Compute Engine machine running Container-Optimized OS from Google (image family `cos-cloud`). The instance with the latest version of Container-Optimized OS is created and the user-specified container (docker image created on the step 3) is pulled and executed once the VM starts. The disk size of the VM is estimated based on the input file size.

Finally, deploy function to Cloud using command:
    
```
gcloud functions deploy trigger_analysis \
--source ./src/ \
--runtime python37 \
--set-env-vars OUTPUT_BUCKET=<OUTPUT_BUCKET>,INSTANCE_ZONE=<INSTANCE_ZONE>,DOCKER_IMAGE=<DOCKER_IMAGE> \
--trigger-resource <BUCKET_NAME> \
--trigger-event "google.storage.object.finalize" \
--region <FUNCTION_REGION> \
--ingress-settings "internal-only" \
--project <PROJECT_ID>
```
Command creates python37 Cloud Function called `trigger_analysis` in the region `<FUNCTION_REGION>` and Google Cloud project `<PROJECT_ID>` with the script source located in `./src/main.py` file. The function trigger is set for the bucket `<BUCKET_NAME>` with the trigger event `google.storage.object.finalize` meaning that once a file is added to the bucket (or re-written), the Cloud Function `trigger_analysis` will be activated. The deployed function has the following environmental variables:
- `<OUTPUT_BUCKET>`: is the name of the Cloud Storage ouput bucket;
- `<INSTANCE_ZONE>`: is the name of the Compute Engine instance;
- `<DOCKER_IMAGE>`: is the name of the Container Registry docker image used for analysing the data;

**5. Copy test file to the bucket**:
```
$ gsutil cp <FILENAME>.fastq.gz gs://<INPUT_BUCKET_NAME>
```

**6. Check outputs.** Once the analysis is finished, the results will be saved to the `<OUTPUT_BUCKET>`.

