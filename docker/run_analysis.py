import os
import json
import glob
import multiqc
import datetime
import argparse
import subprocess
from google.cloud import storage 
from googleapiclient import discovery


def main():

    # parse the arguments
    parser = argparse.ArgumentParser(description='')

    # list either modules or runs
    parser.add_argument('-f', '--file_name', required=True)

    parser.add_argument('-b', '--bucket_name', required=True)

    parser.add_argument('-i', '--instance_name', required=True)

    parser.add_argument('-o', '--output_bucket', required=True)

    parser.add_argument('-z', '--zone', required=True)

    parser.add_argument('-id', '--project_id', required=True)

    # initialize input variables
    args = vars(parser.parse_args())
    file_name = args['file_name']
    bucket_name = args['bucket_name']
    output_bucket_name = args['output_bucket']
    instance_name = args['instance_name']
    zone = args['zone']
    project_id = args['project_id']

    print("Running script using file_name: %s, bucket_name: %s, output_bucket_name: %s, instance_name: %s, zone: %s" % 
        (file_name, bucket_name, output_bucket_name, instance_name, zone))

    # inititalize clients
    client = storage.Client()
    source_bucket = client.get_bucket(bucket_name)
    stamp = datetime.datetime.now().strftime('%d%m%YT%H%M%S')
    output_gcs_multiqc = "%s-%s/multiqc_data" % ('analysis', stamp)
    output_gcs_fastqc = "%s-%s/fastqc" % ('analysis', stamp)

    # donload blob to the vm
    folder='/imported_data'
    if not os.path.exists(folder):
        print("Making a new folder: %s" % folder)
        os.makedirs(folder)

    fastqc_results_dir = "/fastqc_results"
    if not os.path.exists(fastqc_results_dir):
        print("Making a new folder: %s" % fastqc_results_dir)
        os.makedirs(fastqc_results_dir)

    # file localization
    blob = source_bucket.get_blob(file_name)    
    destination_uri = '%s/%s' % (folder, blob.name) 
    
    # localize file to the VM
    print("Obtained blob from the bucket %s : %s. Save file to %s. Localize to the VM" % 
            (source_bucket, file_name, destination_uri))
    blob.download_to_filename(destination_uri)    
    
    print("Copied file to %s. Blob %s" % (destination_uri, blob))
    print('RUNNING INSTANCE %s in zone %s analysing %s. File localized to %s' % 
        (instance_name, zone, file_name, destination_uri))

    print("RUN fastqc")
    try:
        output = subprocess.check_output(["fastqc", destination_uri, "--outdir", fastqc_results_dir],
                                         stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        print("Error: %s" % str(e.output))

    print("RUN multiqc")
    try:
        multiqc.run("/fastqc_results")
    except Exception as e:
        print("ERROR :: %s" % e)

    print('COPY OUTPUTS FROM THE VM TO THE DESTINATION %s' % output_gcs_multiqc)
    try:
        copy_local_dir_to_gcs(client, './multiqc_data', output_bucket_name, output_gcs_multiqc)
    except Exception as e:
        print("ERROR :: %s" % e)

    print('COPY OUTPUTS FROM THE VM TO THE DESTINATION %s' % output_gcs_fastqc)
    try:
        copy_local_dir_to_gcs(client, './fastqc_results', output_bucket_name, output_gcs_fastqc)
    except Exception as e:
        print("ERROR :: %s" % e)

    # copy the main result
    source_file = './multiqc_report.html'
    destination_blob_name_report = "%s/multiqc_report.html" % output_gcs_multiqc
    print("COPY FILE %s TO THE DESTINATION %s" % (source_file, destination_blob_name_report))
    output_bucket = client.get_bucket(output_bucket_name)

    # try to copy the file to the folder
    try:
        blob = output_bucket.blob(destination_blob_name_report)
        blob.upload_from_filename('./multiqc_report.html')
    except Exception as e:
        print("ERROR uploading from filename :: %s" % e)

    print('TERMINATE THE INSTANCE %s FROM INSIDE THE GCE VM' % instance_name)

    if project_id is not None and zone is not None and instance_name is not None:
        res = delete_vm(project_id, zone, instance_name)
        print(res)
    else:
        print("Some of the variables are not present in the env: %s, %s, %s" %
              (project_id, zone, instance_name))


def copy_local_dir_to_gcs(client, local_path, bucket_name, gcs_path):
    bucket = client.get_bucket(bucket_name)
    assert os.path.isdir(local_path)
    for local_file in glob.glob(local_path + '/**'):
        if not os.path.isfile(local_file):
            continue
        remote_path = os.path.join(gcs_path, local_file[1 + len(local_path) :])
        blob = bucket.blob(remote_path)
        blob.upload_from_filename(local_file)


def delete_vm(project_id, zone, instance_name):

    # get the instance
    print("Inside the 'delete_vm' function.")

    compute = discovery.build('compute', 'v1')
    instance = compute.instances().get(instance=instance_name, project=project_id, zone=zone).execute()

    print("Instance found: %s. The details of instance:" % instance_name)
    print(json.dumps(instance, indent=2))

    if instance is not None:
        return compute.instances().delete(project=project_id, zone=zone, instance=instance_name).execute()


if __name__== "__main__":
    main()